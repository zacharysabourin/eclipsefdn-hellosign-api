# eclipsefdn-hellosign-api

Description of business need fulfilled by API goes here.

[[_TOC_]]

## Application details

|Service name|Internal port|Host port|
|-|-|-|
|Application|8090|10300|
|MariaDB|3306|10301|
|FoundationDB API|8095|10302|

## Getting started

### Requirements

* Docker
* mvn
* make
* yarn
* Java 11 >

### Setup

Additional setup instructions will be added as the project grows and evolves.

#### Initial setup

// TODO - depends on project, varies too widely to be easily templated

#### Build and start server

```bash
make compile-start
```

#### Live coding dev mode

```bash
make dev-start
```

#### Generate spec

```bash
make generate-spec
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-hellosign-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-hellosign-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-hellosign-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

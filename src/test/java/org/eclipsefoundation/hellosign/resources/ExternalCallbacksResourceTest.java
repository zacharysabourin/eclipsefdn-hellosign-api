/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.daos.EclipsePersistenceDao;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSignatureEvent;
import org.eclipsefoundation.hellosign.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.hellosign.test.api.MockFoundationDbApi;
import org.eclipsefoundation.hellosign.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.templates.RestAssuredTemplates.EndpointTestCase;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mockito;

import com.dropbox.sign.ApiException;
import com.dropbox.sign.api.SignatureRequestApi;
import com.dropbox.sign.model.EventCallbackRequest;
import com.dropbox.sign.model.EventCallbackRequestEvent;
import com.dropbox.sign.model.EventCallbackRequestEvent.EventTypeEnum;
import com.dropbox.sign.model.SignatureRequestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

/**
 * @author Martin Lowe
 *
 */
@QuarkusTest
class ExternalCallbacksResourceTest {
    public final static String API_BASE_URL = "/";
    public final static String CALLBACKS_BASE_URL = API_BASE_URL + "callback";

    public final static List<EventTypeEnum> ignoredEventTypes = Collections
            .unmodifiableList(Arrays
                    .asList(EventTypeEnum.ACCOUNT_CONFIRMED, EventTypeEnum.FILE_ERROR, EventTypeEnum.SIGN_URL_INVALID,
                            EventTypeEnum.TEMPLATE_CREATED, EventTypeEnum.TEMPLATE_ERROR, EventTypeEnum.UNKNOWN_ERROR,
                            EventTypeEnum.CALLBACK_TEST));

    public final static EndpointTestCase POST_EVENTS_CALLBACK_SUCCESS = TestCaseHelper
            .buildSuccessCase(CALLBACKS_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    public final static EndpointTestCase GET_ALL_AUTH_REQUIRED = TestCaseHelper
            .prepareTestCase(CALLBACKS_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(401)
            .build();

    @InjectMock
    SignatureRequestApi reqApi;

    @Inject
    HellosignTemplatingConfiguration config;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    EclipsePersistenceDao eDao;
    @Inject
    FilterService filters;
    @Inject
    @RestClient
    MockFoundationDbApi fdnMock;

    @Inject
    ObjectMapper mapper;

    @BeforeEach
    void initMocks() throws ApiException, IOException {
        Mockito
                .when(reqApi.signatureRequestFiles("123", "pdf"))
                .thenReturn(writeTempFile("sample content".getBytes(), "sample"));
        Mockito
                .when(reqApi.signatureRequestFiles("completion-test-1", "pdf"))
                .thenReturn(writeTempFile("sample content".getBytes(), "sample"));
    }

    @ParameterizedTest
    @ArgumentsSource(value = RecordedEventTypes.class)
    void hellosignRequestEventsCallback_success(EventTypeEnum type) {
        String requestId = "123";
        EndpointTestBuilder.from(POST_EVENTS_CALLBACK_SUCCESS).doPost(convertToJsonString(get(requestId, type))).run();

        // check that we recorded no event data for current event
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, requestId);
        params.add(SignatureAPIParameterNames.EVENT_TYPE_NAME_RAW, type.getValue());
        Assertions
                .assertFalse(
                        dao
                                .get(new RDBMSQuery<>(
                                        new FlatRequestWrapper(
                                                URI.create("http://test.eclipse.org/foundation/hellosign")),
                                        filters.get(DocumentSignatureEvent.class), params))
                                .isEmpty());
    }

    @ParameterizedTest
    @ArgumentsSource(value = SkippedEventsTypes.class)
    void hellosignRequestEventsCallback_success_skipped(EventTypeEnum type) {
        String requestId = "123";
        EndpointTestBuilder.from(POST_EVENTS_CALLBACK_SUCCESS).doPost(convertToJsonString(get(requestId, type))).run();

        // check that we recorded no event data for current event
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, requestId);
        params.add(SignatureAPIParameterNames.EVENT_TYPE_NAME_RAW, type.getValue());
        Assertions
                .assertTrue(
                        dao
                                .get(new RDBMSQuery<>(
                                        new FlatRequestWrapper(
                                                URI.create("http://test.eclipse.org/foundation/hellosign")),
                                        filters.get(DocumentSignatureEvent.class), params))
                                .isEmpty());
    }

    @Test
    void hellosignRequestEventsCallback_success_signed() {
        String requestId = "completion-test-1";
        // get the doc IDs associated with the current request
        DocumentSignatureTemplate template = config.templates().get("1");
        List<String> assocIds = template
                .relatedDocuments()
                .stream()
                .map(dr -> dr.message().orElse(dr.id()))
                .collect(Collectors.toList());

        // check that there is no CLA request entry for test user
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(SignatureAPIParameterNames.TOKEN_NAME_RAW, "CLA_SIGNED");
        params.add(SignatureAPIParameterNames.EMAIL_NAME_RAW, "oli.pearson@eclipse.org");
        Assertions
                .assertTrue(
                        eDao
                                .get(new RDBMSQuery<>(
                                        new FlatRequestWrapper(
                                                URI.create("http://test.eclipse.org/foundation/hellosign")),
                                        filters.get(AccountRequests.class), params))
                                .isEmpty());
        // pretest assertions to ensure state for current test (no og doc or related docs)
        Assertions
                .assertTrue(fdnMock.pplDocs
                        .stream()
                        .noneMatch(pd -> MockFoundationDbApi.PEARSON_PERSON_ID.equals(pd.getPersonID())
                                && (assocIds.contains(pd.getDocumentID())
                                        || template.document().id().equals(pd.getDocumentID()))));
        // check the modlog state before running
        Assertions
                .assertTrue(fdnMock.modLogs
                        .stream()
                        .noneMatch(ml -> MockFoundationDbApi.PEARSON_PERSON_ID.equals(ml.getPK1())
                                && (assocIds.contains(ml.getPK2()) || template.document().id().equals(ml.getPK2()))),
                        "We should have 0 update event for the request before running the request");

        // generate and run the test with the type tied to the completion workflow
        EndpointTestBuilder
                .from(POST_EVENTS_CALLBACK_SUCCESS)
                .doPost(convertToJsonString(get(requestId, EventTypeEnum.SIGNATURE_REQUEST_ALL_SIGNED)))
                .run();

        // check that the main document was successfully created
        Assertions
                .assertTrue(
                        fdnMock.pplDocs
                                .stream()
                                .anyMatch(pd -> MockFoundationDbApi.PEARSON_PERSON_ID.equals(pd.getPersonID())
                                        && template.document().id().equals(pd.getDocumentID())),
                        "Expected the template document to have been created for test user");
        // related documents should only created if they previously existed
        Assertions
                .assertTrue(
                        fdnMock.pplDocs
                                .stream()
                                .noneMatch(pd -> MockFoundationDbApi.PEARSON_PERSON_ID.equals(pd.getPersonID())
                                        && assocIds.contains(pd.getDocumentID())),
                        "Expected the related template document to have not been created for test user");
        // Check that we are properly sending log events
        Assertions
                .assertEquals(1, fdnMock.modLogs
                        .stream()
                        .filter(ml -> MockFoundationDbApi.PEARSON_PERSON_ID.equals(ml.getPK1()) && (assocIds
                                .contains(ml.getPK2())
                                || template.document().message().orElse(template.document().id()).equals(ml.getPK2())))
                        .count(), "We should have 1 update event for the current request");
        // check that we recorded a CLA signature account update event request
        Assertions
                .assertFalse(
                        eDao
                                .get(new RDBMSQuery<>(
                                        new FlatRequestWrapper(
                                                URI.create("http://test.eclipse.org/foundation/hellosign")),
                                        filters.get(AccountRequests.class), params))
                                .isEmpty());
    }

    /**
     * Generates a stub event callback object with faked data for testing. This will not typically pass the request
     * validation step if enabled.
     * 
     * @param signatureEventId the ID for the current request to set
     * @param eventType the type of event to stub
     * @return the prepared event callback stub to test webhook
     */
    private EventCallbackRequest get(String signatureEventId, EventTypeEnum eventType) {
        return new EventCallbackRequest()
                .event(new EventCallbackRequestEvent().eventType(eventType))
                .signatureRequest(new SignatureRequestResponse().signatureRequestId(signatureEventId));
    }

    /**
     * Required for body of requests in tests, as RESTassured doesn't respect the config set for how data is serialized.
     * 
     * @param o the object to serialize
     * @return the serialized JSON string
     */
    private String convertToJsonString(Object o) {
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
        }
        return null;
    }

    private File writeTempFile(byte[] contents, String name) throws IOException {
        Path temp = Files.createTempFile(name, ".pdf");
        try (FileOutputStream fos = new FileOutputStream(temp.toFile())) {
            fos.write(contents);
            return temp.toFile();
        } catch (IOException e) {
            throw new RuntimeException("Error while prepping test file contents", e);
        }
    }

    /**
     * Arg provider that returns Hellosign Event Types that are recorded by the database on submission.
     * 
     * @author Martin Lowe
     *
     */
    public static class RecordedEventTypes implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream
                    .of(EventTypeEnum.values())
                    .filter(e -> !ignoredEventTypes.contains(e)
                            && !EventTypeEnum.SIGNATURE_REQUEST_ALL_SIGNED.equals(e))
                    .map(Arguments::of);
        }
    }

    /**
     * Arg provider that returns Hellosign Event Types that are skipped/not processed on submission.
     * 
     * @author Martin Lowe
     *
     */
    public static class SkippedEventsTypes implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return ignoredEventTypes.stream().map(Arguments::of);
        }
    }
}

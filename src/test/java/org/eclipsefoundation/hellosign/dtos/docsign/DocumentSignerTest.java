/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * 
 */
@QuarkusTest
class DocumentSignerTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    @Test
    void hasFilter() {
        Assertions.assertNotNull(filters.get(DocumentSigner.class));
    }

    @Test
    void filter_success_id() {
        DocumentSigner sample = getTestInstance("");
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<DocumentSigner> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(DocumentSigner.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        DocumentSigner expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(expected.getId()));
        List<DocumentSigner> results = dao.get(new RDBMSQuery<>(wrap, filters.get(DocumentSigner.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    private DocumentSigner getTestInstance(String requestId) {
        DocumentSigner test = new DocumentSigner();
        test.setCreated(DateTimeHelper.now());
        test.setMail("");
        test.setName("");
        test.setUsername("");
        test.setRole("");
        test.setRequestId(requestId);
        return test;
    }
}

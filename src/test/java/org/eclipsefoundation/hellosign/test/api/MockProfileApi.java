/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import io.quarkus.arc.Priority;

/**
 * @author martin
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockProfileApi implements ProfileAPI {

    List<EfUser> users;

    @PostConstruct
    void init() {
        this.users = new ArrayList<>();
        this.users.add(buildBase().setName("opearson").setMail("oli.pearson@eclipse.org").build());
    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams arg1) {
        return users
                .stream()
                .filter(u -> StringUtils.isBlank(arg1.getMail()) || u.getMail().equals(arg1.getMail()))
                .filter(u -> StringUtils.isBlank(arg1.getName()) || u.getName().equals(arg1.getName()))
                .filter(u -> arg1.getUid() == null || u.getUid() == arg1.getUid())
                .collect(Collectors.toList());
    }

    private EfUser.Builder buildBase() {
        return EfUser
                .builder()
                .setUid(666)
                .setName("firstlast")
                .setGithubHandle("handle")
                .setMail("firstlast@test.com")
                .setPicture("pic url")
                .setFirstName("fake")
                .setLastName("user")
                .setPublisherAgreements(new HashMap<>())
                .setTwitterHandle("")
                .setOrg("null")
                .setJobTitle("employee")
                .setWebsite("site url")
                .setCountry(Country.builder().build())
                .setInterests(Arrays.asList())
                .setWorkingGroupsInterests(Arrays.asList());
    }

    @Override
    public EfUser getUserByEfUsername(String arg0, String arg1) {
        return users.stream().filter(u -> u.getName().equals(arg1)).findFirst().orElse(null);
    }

    @Override
    public EfUser getUserByGithubHandle(String arg0, String arg1) {
        return users.stream().filter(u -> u.getGithubHandle().equals(arg1)).findFirst().orElse(null);
    }

}

/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.test.api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.hellosign.apis.FoundationDbAPI;

import io.quarkus.arc.Priority;

/**
 * @author Martin Lowe
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockFoundationDbApi implements FoundationDbAPI {

    public static final String PEARSON_PERSON_ID = "opearson";

    // make the test storage visible to test for presence of values more easily
    public List<PeopleDocumentData> pplDocs;
    public List<OrganizationDocumentData> orgDocs;
    public List<PeopleData> ppl;
    public List<OrganizationContactData> orgConacts;
    public List<SysModLogData> modLogs;

    @PostConstruct
    void init() {
        this.ppl = new ArrayList<>();
        this.ppl
                .add(PeopleData
                        .builder()
                        .setEmail("oli.pearson@eclipse.org")
                        .setPersonID(PEARSON_PERSON_ID)
                        .setFname("first name")
                        .setLname("last name")
                        .setType("member")
                        .setMember(false)
                        .setUnixAcctCreated(false)
                        .setIssuesPending(false)
                        .build());
        this.pplDocs = new ArrayList<>();
        this.orgConacts = new ArrayList<>();
        this.orgConacts
                .add(OrganizationContactData
                        .builder()
                        .setPersonID(PEARSON_PERSON_ID)
                        .setOrganizationID(1)
                        .setRelation("CR")
                        .setTitle("")
                        .setDocumentID("")
                        .build());
        this.orgDocs = new ArrayList<>();
        this.modLogs = new ArrayList<>();
    }

    @Override
    public Response getOrganizationContact(BaseAPIParameters params, String username, String relation,
            Integer organizationId) {
        return Response
                .ok(orgConacts
                        .stream()
                        .filter(oc -> username == null || oc.getPersonID().equals(username))
                        .filter(oc -> relation == null || oc.getRelation().equals(relation))
                        .filter(oc -> organizationId == null || oc.getOrganizationID() == organizationId)
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Response getOrganizationDocuments(BaseAPIParameters params, Integer organizationId,
            List<String> documentIds) {
        return Response
                .ok(this.orgDocs
                        .stream()
                        .filter(od -> organizationId == null || od.getOrganizationID() == organizationId)
                        .filter(od -> documentIds == null || documentIds.isEmpty()
                                || documentIds.contains(od.getDocumentID()))
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public PeopleData getPersonRecord(String username) {
        return this.ppl.stream().filter(p -> p.getPersonID().equals(username)).findFirst().orElse(null);
    }

    @Override
    public Response getPeopleDocuments(BaseAPIParameters params, List<String> docIds, List<String> peopleIds) {
        return Response
                .ok(this.pplDocs
                        .stream()
                        .filter(pd -> peopleIds == null || peopleIds.isEmpty() || peopleIds.contains(pd.getPersonID()))
                        .filter(pd -> docIds == null || docIds.isEmpty() || docIds.contains(pd.getDocumentID()))
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Response getPeopleDocumentsForUser(BaseAPIParameters params, String username, List<String> docIds) {
        return Response
                .ok(this.pplDocs
                        .stream()
                        .filter(pd -> username == null || username.equals(pd.getPersonID()))
                        .filter(pd -> docIds == null || docIds.isEmpty() || docIds.contains(pd.getDocumentID()))
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Response updatePeopleDocument(String username, PeopleDocumentData payload) {
        this.pplDocs
                .removeIf(
                        pd -> pd.getDocumentID().equals(payload.getDocumentID()) && pd.getPersonID().equals(username));
        this.pplDocs.add(payload);
        return Response.ok(List.of(payload)).build();
    }

    @Override
    public Response updateOrganizationDocument(Integer organizationId, OrganizationDocumentData payload) {
        this.orgDocs
                .removeIf(od -> od.getDocumentID().equals(payload.getDocumentID())
                        && od.getOrganizationID() == organizationId);
        this.orgDocs.add(payload);
        return Response.ok(List.of(payload)).build();
    }

    @Override
    public List<PeopleData> updatePeople(PeopleData payload) {
        this.ppl.removeIf(p -> p.getPersonID().equals(payload.getPersonID()));
        this.ppl.add(payload);
        return List.of(payload);
    }

    @Override
    public List<SysModLogData> updateModlog(SysModLogData payload) {
        this.modLogs.add(payload);
        return List.of(payload);
    }

}

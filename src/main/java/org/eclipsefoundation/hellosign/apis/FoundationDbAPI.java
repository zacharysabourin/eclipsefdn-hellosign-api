/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.apis;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.security.Authenticated;

/**
 * Interface for interacting with FoundationDB API.
 * 
 * @author Martin Lowe
 *
 */
@OidcClientFilter
@Path("")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationDbAPI {

    @GET
    @RolesAllowed({ "fdb_read_people", "fdb_read_organization", "fdb_read_organization_employment" })
    @Path("organizations/contacts/full")
    Response getOrganizationContact(@BeanParam BaseAPIParameters params, @QueryParam("personID") String username,
            @QueryParam("relation") String relation, @QueryParam("organization_id") Integer organizationId);

    @GET
    @RolesAllowed("fdb_read_organizations_documents")
    @Path("organizations/{organization_id}/documents")
    Response getOrganizationDocuments(@BeanParam BaseAPIParameters params,
            @PathParam("organization_id") Integer organizationId, @QueryParam("doc_ids") List<String> documentIds);

    @GET
    @RolesAllowed("fdb_read_people")
    @Path("people/{user}")
    PeopleData getPersonRecord(@PathParam("user") String username);

    @GET
    @RolesAllowed("fdb_read_people_documents")
    @Path("people/documents")
    Response getPeopleDocuments(@BeanParam BaseAPIParameters params, @QueryParam("document_ids") List<String> docIds,
            @QueryParam("people_ids") List<String> peopleIds);

    @GET
    @RolesAllowed("fdb_read_people_documents")
    @Path("people/{user}/documents")
    Response getPeopleDocumentsForUser(@BeanParam BaseAPIParameters params, @PathParam("user") String username,
            @QueryParam("document_ids") List<String> docIds);

    @PUT
    @RolesAllowed("fdb_write_people_documents")
    @Path("people/{user}/documents")
    Response updatePeopleDocument(@PathParam("user") String username, PeopleDocumentData payload);

    @PUT
    @RolesAllowed("fdb_write_organizations_documents")
    @Path("organizations/{organization_id}/documents")
    Response updateOrganizationDocument(@PathParam("organization_id") Integer organizationId,
            OrganizationDocumentData payload);

    @PUT
    @RolesAllowed("fdb_write_people")
    @Path("people")
    List<PeopleData> updatePeople(PeopleData payload);

    @PUT
    @RolesAllowed("fdb_write_sys_modlog")
    @Path("sys/mod_logs")
    List<SysModLogData> updateModlog(SysModLogData payload);
}

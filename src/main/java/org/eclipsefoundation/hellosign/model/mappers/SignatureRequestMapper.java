/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.model.mappers;

import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.hellosign.model.EfHellosignRequestData;
import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper to convert the stored signature requests into the API response body.
 * 
 * @author Martin Lowe
 *
 */
@Mapper(config = QuarkusMappingConfig.class)
public interface SignatureRequestMapper extends BaseEntityMapper<SignatureRequest, EfHellosignRequestData> {

    @Mapping(source = "upstreamResponse", target = "response")
    @Mapping(source = "requestTemplateId", target = "templateId")
    @Mapping(source = "requestId", target = "hellosignSignatureId")
    @Mapping(source = "id", target = "requestId")
    EfHellosignRequestData toModel(SignatureRequest request);
}

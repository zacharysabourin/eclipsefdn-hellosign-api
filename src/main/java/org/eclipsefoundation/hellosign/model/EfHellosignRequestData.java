/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.hellosign.model;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Signature request information model.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_EfHellosignRequestData.Builder.class)
public abstract class EfHellosignRequestData {

    public abstract String getRequestId();

    public abstract String getHellosignSignatureId();

    public abstract String getTemplateId();

    public abstract String getTitle();

    public abstract String getSubject();

    public abstract String getMessage();

    public abstract String getResponse();

    public abstract ZonedDateTime getCreated();

    @Nullable
    public abstract List<HellosignRequestEvents> getEvents();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_EfHellosignRequestData.Builder().setEvents(Collections.emptyList());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setRequestId(String requestId);

        public abstract Builder setHellosignSignatureId(String hellosignSignatureId);

        public abstract Builder setTemplateId(String templateId);

        public abstract Builder setTitle(String title);

        public abstract Builder setSubject(String subject);

        public abstract Builder setMessage(String message);

        public abstract Builder setResponse(String response);

        public abstract Builder setCreated(ZonedDateTime response);

        public abstract Builder setEvents(@Nullable List<HellosignRequestEvents> response);

        public abstract EfHellosignRequestData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfHellosignRequestData_HellosignRequestEvents.Builder.class)
    public abstract static class HellosignRequestEvents {

        public abstract Long getId();

        public abstract String getBody();

        public abstract String getEventType();

        public abstract String getRequestId();

        public abstract ZonedDateTime getCreated();

        public static Builder builder() {
            return new AutoValue_EfHellosignRequestData_HellosignRequestEvents.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setId(Long id);

            public abstract Builder setBody(String body);

            public abstract Builder setEventType(String eventType);

            public abstract Builder setRequestId(String requestId);

            public abstract Builder setCreated(ZonedDateTime created);

            public abstract HellosignRequestEvents build();
        }
    }
}

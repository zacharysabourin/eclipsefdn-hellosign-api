/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services;

import org.eclipsefoundation.hellosign.model.EfHellosignRequestData;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest;

/**
 * Interface describing how document signature request workflows are managed.
 * 
 * @author Martin Lowe
 *
 */
public interface DocumentSignatureService {

    /**
     * Generates a new signature request based on the passed request body
     * 
     * @param request the incoming request to generate signature requests
     * @return the newly generated signature request record
     */
    EfHellosignRequestData createNewSignatureRequest(HellosignSignatureRequest request);

    /**
     * Using the upstream request ID, fetch and complete the associated request.
     * 
     * @param upstreamId the upstream document signature ID to use when looking up the request records
     */
    void completeSignatureRequest(String upstreamId);
}

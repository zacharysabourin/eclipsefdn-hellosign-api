/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services;

import org.eclipsefoundation.hellosign.model.BadStateNotificationParams;

/**
 * Interface for service that builds and sends mail to the EMO team regarding bad state.
 * 
 * @author Martin Lowe
 *
 */
public interface MailerService {

    /**
     * Sends a message to EMO using the provided params about a bad request.
     * 
     * @param params the parameters for the current bad request
     */
    void sendMailToEMO(BadStateNotificationParams params);
}

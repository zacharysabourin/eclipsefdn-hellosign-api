/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ServerErrorException;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSignatureEvent;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSigner;
import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.sign.ApiException;
import com.dropbox.sign.EventCallbackHelper;
import com.dropbox.sign.api.SignatureRequestApi;
import com.dropbox.sign.model.EventCallbackRequest;
import com.dropbox.sign.model.SignatureRequestGetResponse;
import com.dropbox.sign.model.SignatureRequestResponse;
import com.dropbox.sign.model.SignatureRequestSendWithTemplateRequest;
import com.dropbox.sign.model.SubSignatureRequestTemplateSigner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Implementation of the binding to the Hellosign service, using the SDK provided by Dropbox to action requests for
 * information and new signature requests.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultHellosignBindingService implements HellosignBindingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultHellosignBindingService.class);

    @Inject
    HellosignTemplatingConfiguration config;

    @ConfigProperty(name = "eclipse.hellosign.application-token")
    String token;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Inject
    DocumentSignatureService signingService;
    @Inject
    CachingService cache;

    @Inject
    ObjectMapper om;
    @Inject
    SignatureRequestApi signatureRequest;

    @Override
    public Byte[] getDocumentForRequest(String requestId) {
        // attempt to fetch PDF, allowing failures in API or ingest to trickle down
        Optional<Byte[]> pdfContents = cache.get(requestId, null, Byte.class, () -> {
            // read the file, failing out if the request could not get file contents
            File f = this.signatureRequest.signatureRequestFiles(requestId, "pdf");
            if (f == null) {
                return null;
            }
            // will throw an IO exception if the file is not properly created
            try (FileInputStream fis = new FileInputStream(f)) {
                return ArrayUtils.toObject(fis.readAllBytes());
            }
        });
        // check the error state and throw if the document couldn't be retrieved
        if (pdfContents.isEmpty()) {
            throw new ServerErrorException(
                    "Error while retrieving PDF contents for current request, the document may not be available", 500);
        }
        return pdfContents.get();
    }

    @Override
    public SignatureRequestResponse createSignatureRequest(DocumentSignatureTemplate template, SignatureRequest request,
            List<DocumentSigner> signers) {
        try {
            // build and submit the request for signature, using the passed data to build the full Hellosign request
            SignatureRequestGetResponse r = signatureRequest
                    .signatureRequestSendWithTemplate(new SignatureRequestSendWithTemplateRequest()
                            .templateIds(List.of(template.templateId()))
                            .title(template.templateName() + (config.testMode() ? " - TEST" : ""))
                            .subject(request.getSubject())
                            .message(request.getMessage())
                            .signers(signers
                                    .stream()
                                    .map(signer -> new SubSignatureRequestTemplateSigner()
                                            .role(signer.getRole())
                                            .emailAddress(signer.getMail())
                                            .name(signer.getName()))
                                    .collect(Collectors.toList()))
                            .testMode(config.testMode()));
            // log the success and return the response when complete
            if (LOGGER.isDebugEnabled()) {
                LOGGER
                        .debug("Created signature request for template {} with signers '{}' (testing? {})",
                                template.templateName(),
                                signers.stream().map(DocumentSigner::getMail).collect(Collectors.toList()),
                                config.testMode());
            }
            return r.getSignatureRequest();
        } catch (ApiException e) {
            LOGGER.error("Error while interacting with Hellosign API: ", e);
            // throw a new server exception that bubbles up
            throw new ServerErrorException("Error while creating new signature request", 500);
        }
    }

    @Override
    public boolean validateIncomingRequest(EventCallbackRequest payload) {
        // use the helper from hellosign to validate incoming event hash
        if (config.enableRequestValidation() && !EventCallbackHelper.isValid(config.applicationToken(), payload)) {
            throw new FinalForbiddenException("Incoming payload not recognized as legitimate, rejecting request");
        }
        return true;
    }

    @Override
    public void processIncomingDocumentEvent(EventCallbackRequest payload) {
        // convert the necessary event data into an event record to persist
        DocumentSignatureEvent dse = new DocumentSignatureEvent();
        dse.setCreated(DateTimeHelper.now());
        dse.setEventType(payload.getEvent().getEventType().getValue());
        dse.setRequestId(payload.getSignatureRequest().getSignatureRequestId());
        dse.setEventBody(config.applicationToken());
        // serialize the entire event callback for records
        dse.setEventBody(serializeEventCallback(payload));

        // persist the new record
        List<DocumentSignatureEvent> updatedRecords = dao
                .add(new RDBMSQuery<>(
                        new FlatRequestWrapper(URI.create("https://api.eclipse.org/foundation/hellosign")),
                        filters.get(DocumentSignatureEvent.class)), Arrays.asList(dse));
        if (updatedRecords.isEmpty()) {
            LOGGER
                    .error("Error while persisting event callback of type {} for request {}",
                            payload.getEvent().getEventType().getValue(),
                            payload.getSignatureRequest().getSignatureRequestId());
        }
    }

    /**
     * Serializes the signature request response from Hellosign, converting it into JSON data to be stored along with
     * the request record.
     * 
     * @param r the response from Hellosign for creating the signature request
     * @return the JSON data for the response, or null if it can't be converted
     */
    private String serializeEventCallback(EventCallbackRequest r) {
        try {
            return om.writeValueAsString(r);
        } catch (JsonProcessingException e) {
            LOGGER
                    .error("Error processing event callback for request {}",
                            r.getSignatureRequest().getSignatureRequestId());
            return null;
        }
    }
}

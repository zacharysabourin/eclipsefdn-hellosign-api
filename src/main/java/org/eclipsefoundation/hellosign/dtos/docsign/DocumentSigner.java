/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.time.ZonedDateTime;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.hellosign.config.DateTimeAttributeConverter;
import org.eclipsefoundation.hellosign.helpers.DateTimeHelper;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * @author Martin Lowe
 *
 */
@Entity
@Table(name = "shared_eclipse_api_hellosign_signer")
public class DocumentSigner extends BareNode {
    public static final DtoTable TABLE = new DtoTable(DocumentSigner.class, "ds");

    @Id
    @Column(name = "sid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "signature_request_id")
    private String requestId;
    @Column(name = "signer_role")
    private String role;
    @Column(name = "signer_mail")
    private String mail;
    @Column(name = "signer_name")
    private String name;
    @Column(name = "signer_person_id")
    private String username;
    @Convert(converter = DateTimeAttributeConverter.class)
    private ZonedDateTime created;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the created
     */
    public ZonedDateTime getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(created, id, mail, name, requestId, role, username);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DocumentSigner other = (DocumentSigner) obj;
        return DateTimeHelper.looseCompare(created, other.created) && Objects.equals(id, other.id)
                && Objects.equals(mail, other.mail) && Objects.equals(name, other.name)
                && Objects.equals(requestId, other.requestId) && Objects.equals(role, other.role)
                && Objects.equals(username, other.username);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DocumentSigner [id=");
        builder.append(id);
        builder.append(", requestId=");
        builder.append(requestId);
        builder.append(", role=");
        builder.append(role);
        builder.append(", mail=");
        builder.append(mail);
        builder.append(", name=");
        builder.append(name);
        builder.append(", username=");
        builder.append(username);
        builder.append(", created=");
        builder.append(created);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class DocumentSignerFilter implements DtoFilter<DocumentSigner> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);
            String id = params.getFirst(DefaultUrlParameterNames.ID_PARAMETER_NAME);
            if (StringUtils.isNumeric(id)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                new Object[] { Long.parseLong(id) }));
            }

            return statement;
        }

        @Override
        public Class<DocumentSigner> getType() {
            return DocumentSigner.class;
        }
    }
}

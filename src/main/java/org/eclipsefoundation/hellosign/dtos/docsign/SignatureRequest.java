/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.time.ZonedDateTime;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.hellosign.config.DateTimeAttributeConverter;
import org.eclipsefoundation.hellosign.helpers.DateTimeHelper;
import org.eclipsefoundation.hellosign.namespaces.HellosignRequestStatus;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * Contains information about signature requests created through this API.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table(name = "shared_eclipse_api_hellosign_request")
public class SignatureRequest extends BareNode {
    public static final DtoTable TABLE = new DtoTable(SignatureRequest.class, "sr");

    @Id
    @Column(name = "hid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String requestTemplateId;
    @Column(name = "request_title")
    private String title;
    @Column(name = "request_subject")
    private String subject;
    @Column(name = "request_message")
    private String message;
    @Column(name = "signature_request_id")
    private String requestId;
    @Column(name = "response", columnDefinition = "longblob")
    private String upstreamResponse;
    @Convert(converter = DateTimeAttributeConverter.class)
    private ZonedDateTime created;
    @Column(name = "request_status")
    private HellosignRequestStatus status;

    /**
     * @return the id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the requestTemplateId
     */
    public String getRequestTemplateId() {
        return requestTemplateId;
    }

    /**
     * @param requestTemplateId the requestTemplateId to set
     */
    public void setRequestTemplateId(String requestTemplateId) {
        this.requestTemplateId = requestTemplateId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the upstreamResponse
     */
    public String getUpstreamResponse() {
        return upstreamResponse;
    }

    /**
     * @param upstreamResponse the upstreamResponse to set
     */
    public void setUpstreamResponse(String upstreamResponse) {
        this.upstreamResponse = upstreamResponse;
    }

    /**
     * @return the created
     */
    public ZonedDateTime getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    /**
     * @return the status
     */
    public HellosignRequestStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(HellosignRequestStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects
                .hash(created, id, message, requestId, requestTemplateId, status, subject, title, upstreamResponse);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SignatureRequest other = (SignatureRequest) obj;
        return DateTimeHelper.looseCompare(created, other.created) && Objects.equals(id, other.id)
                && Objects.equals(message, other.message) && Objects.equals(requestId, other.requestId)
                && Objects.equals(requestTemplateId, other.requestTemplateId) && status == other.status
                && Objects.equals(subject, other.subject) && Objects.equals(title, other.title)
                && Objects.equals(upstreamResponse, other.upstreamResponse);
    }
    


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SignatureRequest [id=");
        builder.append(id);
        builder.append(", requestTemplateId=");
        builder.append(requestTemplateId);
        builder.append(", title=");
        builder.append(title);
        builder.append(", subject=");
        builder.append(subject);
        builder.append(", message=");
        builder.append(message);
        builder.append(", requestId=");
        builder.append(requestId);
        builder.append(", upstreamResponse=");
        builder.append(upstreamResponse);
        builder.append(", created=");
        builder.append(created);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class SignatureRequestFilter implements DtoFilter<SignatureRequest> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);
            String id = params.getFirst(DefaultUrlParameterNames.ID_PARAMETER_NAME);
            if (StringUtils.isNumeric(id)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                new Object[] { Long.parseLong(id) }));
            }
            // internal request template ID
            String template = params.getFirst(SignatureAPIParameterNames.TEMPLATE_NAME_RAW);
            if (StringUtils.isNotBlank(template)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".requestTemplateId = ?",
                                new Object[] { template }));
            }

            String mail = params.getFirst(SignatureAPIParameterNames.EMAIL_NAME_RAW);
            String role = params.getFirst(SignatureAPIParameterNames.ROLE_NAME_RAW);
            if (StringUtils.isNotBlank(mail) && StringUtils.isNotBlank(role)) {
                statement
                        .addJoin(new ParameterizedSQLStatement.Join(TABLE, DocumentSigner.TABLE, "requestId",
                                "requestId"));
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(DocumentSigner.TABLE.getAlias() + ".mail = ?",
                                new Object[] { mail }));
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(DocumentSigner.TABLE.getAlias() + ".role = ?",
                                new Object[] { role }));
            }

            return statement;
        }

        @Override
        public Class<SignatureRequest> getType() {
            return SignatureRequest.class;
        }
    }
}

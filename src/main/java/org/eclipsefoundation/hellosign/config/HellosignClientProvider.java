/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import com.dropbox.sign.ApiClient;
import com.dropbox.sign.Configuration;
import com.dropbox.sign.api.SignatureRequestApi;
import com.dropbox.sign.auth.HttpBasicAuth;

import io.quarkus.arc.DefaultBean;

/**
 * Adds the Hellosign Signature Request API binding to CDI.
 * 
 * @author Martin Lowe
 *
 */
@Dependent
public class HellosignClientProvider {

    @Produces
    @DefaultBean
    @ApplicationScoped
    public SignatureRequestApi defaultApi(HellosignTemplatingConfiguration config) {
        ApiClient apiClient = Configuration.getDefaultApiClient();
        ((HttpBasicAuth) apiClient.getAuthentication("api_key")).setUsername(config.applicationToken());
        return new SignatureRequestApi(apiClient);
    }
}

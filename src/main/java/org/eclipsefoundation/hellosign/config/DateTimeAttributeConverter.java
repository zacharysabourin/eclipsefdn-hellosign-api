/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.config;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import javax.persistence.AttributeConverter;

import org.eclipsefoundation.core.helper.DateTimeHelper;

/**
 * Provides mapping of epoch milli to UTC zoned datetime objects.
 * 
 * @author Martin Lowe
 *
 */
public class DateTimeAttributeConverter implements AttributeConverter<ZonedDateTime, Long> {

    @Override
    public Long convertToDatabaseColumn(ZonedDateTime attribute) {
        return TimeUnit.SECONDS.convert(DateTimeHelper.getMillis(attribute), TimeUnit.MILLISECONDS);
    }

    @Override
    public ZonedDateTime convertToEntityAttribute(Long dbData) {
        return Instant.ofEpochSecond(dbData).atZone(ZoneId.of("UTC"));
    }

}

/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.namespaces;

import java.util.stream.Stream;

/**
 * @author Martin Lowe
 *
 */
public enum HellosignRequestStatus {
    ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_UNFINISHED(0), ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_FINISHED(1),
    ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_ERROR(2);

    private int code;

    private HellosignRequestStatus(int code) {
        this.code = code;
    }

    public static HellosignRequestStatus getByCode(int code) {
        return Stream.of(values()).filter(s -> s.code == code).findFirst().orElse(null);
    }

    public int getCode() {
        return this.code;
    }
}

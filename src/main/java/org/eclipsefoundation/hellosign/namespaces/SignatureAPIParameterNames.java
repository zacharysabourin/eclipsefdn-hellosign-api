/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.namespaces;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

/**
 * Parameter names used when reading query params into DB query parameters
 * 
 * @author Martin Lowe
 *
 */
public class SignatureAPIParameterNames implements UrlParameterNamespace {
    public static final String REQ_WHEN_NAME_RAW = "req_when";
    public static final String EMAIL_NAME_RAW = "email";
    public static final String ROLE_NAME_RAW = "role";
    public static final String EVENT_TYPE_NAME_RAW = "event_type";
    public static final String TEMPLATE_NAME_RAW = "template";
    public static final String UPSTREAM_REQUEST_ID_RAW = "request_id";
    public static final String TOKEN_NAME_RAW = "token";
    public static final UrlParameter REQ_WHEN = new UrlParameter(REQ_WHEN_NAME_RAW);
    public static final UrlParameter EMAIL = new UrlParameter(EMAIL_NAME_RAW);
    public static final UrlParameter ROLE = new UrlParameter(ROLE_NAME_RAW);
    public static final UrlParameter EVENT_TYPE = new UrlParameter(EVENT_TYPE_NAME_RAW);
    public static final UrlParameter TEMPLATE = new UrlParameter(TEMPLATE_NAME_RAW);
    public static final UrlParameter UPSTREAM_REQUEST_ID = new UrlParameter(UPSTREAM_REQUEST_ID_RAW);
    public static final UrlParameter TOKEN = new UrlParameter(TOKEN_NAME_RAW);

    // Unmodifiable list to use downstream
    public static final List<UrlParameter> PARAMETERS = Collections
            .unmodifiableList(Arrays.asList(REQ_WHEN, EMAIL, UPSTREAM_REQUEST_ID, ROLE, TEMPLATE, EVENT_TYPE, TOKEN));

    @Override
    public List<UrlParameter> getParameters() {
        return PARAMETERS;
    }

}

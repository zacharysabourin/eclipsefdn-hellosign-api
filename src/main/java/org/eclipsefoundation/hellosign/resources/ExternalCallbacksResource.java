/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.sign.model.EventCallbackRequest;

/**
 * Resource for handling external callbacks related to Hellosign signature requests.
 * 
 * @author Martin Lowe
 *
 */
@Path("callback")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExternalCallbacksResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalCallbacksResource.class);

    @Inject
    HellosignBindingService hellosign;
    @Inject
    DocumentSignatureService signatureService;

    @POST
    public Response hellosignRequestEventsCallback(EventCallbackRequest payload) {
        // check that the incoming request is valid and from Hellosign
        hellosign.validateIncomingRequest(payload);

        // handle the different event types (we only care about signature request events)
        switch (payload.getEvent().getEventType()) {
            case SIGNATURE_REQUEST_ALL_SIGNED:
                LOGGER
                        .info("Recieved completion event for request {}, beginning processing",
                                payload.getSignatureRequest().getSignatureRequestId());
                signatureService.completeSignatureRequest(payload.getSignatureRequest().getSignatureRequestId());
                break;
            case CALLBACK_TEST:
                LOGGER.info("Recieved a test ping from Hellosign. Ping!");
                break;
            case ACCOUNT_CONFIRMED:
            case FILE_ERROR:
            case SIGN_URL_INVALID:
            case TEMPLATE_CREATED:
            case TEMPLATE_ERROR:
            case UNKNOWN_ERROR:
                LOGGER
                        .debug("Recieved callback for unsupported event type {}, which is not currently handled by API, dropping without processing",
                                payload.getEvent().getEventType());
                break;
            default:
                hellosign.processIncomingDocumentEvent(payload);
                break;
        }
        return Response.ok().build();
    }

}

/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSignatureEvent;
import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.hellosign.model.EfHellosignRequestData;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest.Signer;
import org.eclipsefoundation.hellosign.model.mappers.DocumentSignatureEventMapper;
import org.eclipsefoundation.hellosign.model.mappers.SignatureRequestMapper;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;

/**
 * Resource containing CRUD endpoints for interacting with Hellosign signature requests made by the system.
 * 
 * @author Martin Lowe
 *
 */
@Path("")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
public class HellosignResources {
    private static final Logger LOGGER = LoggerFactory.getLogger(HellosignResources.class);

    @Inject
    HellosignTemplatingConfiguration templateConfigs;

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;
    @Inject
    CachingService cache;
    @Inject
    HellosignBindingService hellosign;
    @Inject
    DocumentSignatureService signatureService;

    @Inject
    SignatureRequestMapper requestMapper;
    @Inject
    DocumentSignatureEventMapper eventMapper;

    @Inject
    RequestWrapper wrapper;

    @GET
    public List<EfHellosignRequestData> getAllHellosignRequests() {
        // add pagination mappings to handle caching more smoothly
        MultivaluedMap<String, String> paginationParams = new MultivaluedMapImpl<>();
        paginationParams
                .add(DefaultUrlParameterNames.PAGE_PARAMETER_NAME,
                        wrapper.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1"));
        paginationParams
                .add(DefaultUrlParameterNames.PAGESIZE_PARAMETER_NAME,
                        wrapper.getFirstParam(DefaultUrlParameterNames.PAGESIZE).orElseGet(() -> "25"));
        // get all results from the cache
        Optional<List<SignatureRequest>> allResults = cache
                .get("all", paginationParams, SignatureRequest.class,
                        () -> dao.get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class))));
        if (allResults.isEmpty()) {
            LOGGER.warn("Error while fetching all request results, returning empty list");
            return Collections.emptyList();
        }
        // convert the list of results to the JSON model and return
        return allResults.get().stream().map(requestMapper::toModel).collect(Collectors.toList());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public List<EfHellosignRequestData> createNewHellosignRequests(HellosignSignatureRequest request) {
        // get current signature requests for user
        List<SignatureRequest> currentRequestsForUser = dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class),
                        buildDuplicateRequestParams(request)));
        // if there is an inprocess request, don't create another one
        if (!currentRequestsForUser.isEmpty()) {
            throw new BadRequestException("There is already an active request for targeted client user");
        }

        return Arrays.asList(signatureService.createNewSignatureRequest(request));
    }

    @GET
    @Path("{id}")
    public EfHellosignRequestData getHellosignRequestById(@PathParam("id") Long id) {
        return cache
                .get(Long.toString(id), new MultivaluedMapImpl<>(), EfHellosignRequestData.class,
                        () -> getAugmentedRequestData(id))
                .orElseThrow(() -> new NotFoundException("Could not find a signature request with the given ID"));
    }

    @GET
    @Path("{id}/document")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getHellosignRequestDocumentById(@PathParam("id") Long id) {
        // add the ID to a param map, and pass to the RDBMS query to fetch single entry if it exists
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(id));
        SignatureRequest currentResult = dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class), params))
                .stream()
                .limit(1)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Could not find a signature request with the given ID"));

        // get the actual document contents from hellosign
        Byte[] docContents = hellosign.getDocumentForRequest(currentResult.getRequestId());

        // return the temp document as a file stream to the user
        return Response
                .ok(new ByteArrayInputStream(ArrayUtils.toPrimitive(docContents)))
                .header("content-disposition", String.format("attachment; filename=\"%s-current-signature.pdf\"", id))
                .build();
    }

    @GET
    @Path("templates")
    public List<DocumentSignatureTemplate> getAvailableTemplates() {
        return new ArrayList<>(templateConfigs.templates().values());
    }

    /**
     * Build and retrieve the request data for the given Hellosign signature request, augmenting the return with the
     * events associated with the request if any exist.
     * 
     * @param id the internal ID for the signature request to retrieve.
     * @return the augmented request response if it exists, or null
     */
    private EfHellosignRequestData getAugmentedRequestData(Long id) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(id));

        return dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class), params))
                .stream()
                .limit(1)
                .map(r -> {
                    // map to the response model
                    EfHellosignRequestData data = requestMapper.toModel(r);
                    // set up params to fetch events related to the found request
                    MultivaluedMap<String, String> p = new MultivaluedMapImpl<>();
                    p.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, r.getRequestId());

                    // retrieve associated events and set them into the data model
                    return data
                            .toBuilder()
                            .setEvents(dao
                                    .get(new RDBMSQuery<DocumentSignatureEvent>(wrapper,
                                            filters.get(DocumentSignatureEvent.class), p))
                                    .stream()
                                    .map(eventMapper::toModel)
                                    .collect(Collectors.toList()))
                            .build();
                })
                .findFirst()
                .orElse(null);
    }

    private MultivaluedMap<String, String> buildDuplicateRequestParams(HellosignSignatureRequest request) {
        // check that we have a client signer for the request
        Optional<Signer> client = request
                .getSigners()
                .stream()
                .filter(s -> "client".equalsIgnoreCase(s.getRole()))
                .findFirst();
        if (client.isEmpty()) {
            throw new BadRequestException("Client signer required for request");
        }
        // track the needed params to look for duplicate requests
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(SignatureAPIParameterNames.EMAIL_NAME_RAW, client.get().getMail());
        params.add(SignatureAPIParameterNames.ROLE_NAME_RAW, client.get().getRole());
        params.add(SignatureAPIParameterNames.TEMPLATE_NAME_RAW, request.getTemplateId());
        return params;
    }
}

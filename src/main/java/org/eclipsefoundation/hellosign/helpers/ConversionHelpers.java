/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.helpers;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSigner;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest.Signer;

/**
 * Helpers in conversion of data in the API rather than include mapstruct.
 * 
 * @author Martin Lowe
 *
 */
public final class ConversionHelpers {

    /**
     * Converts the list of signers to document signature records to be later persisted.
     * 
     * @param signer list of signatories to convert to document signer records.
     * @return the converted list of document signer records
     */
    public static List<DocumentSigner> convertIncomingSigners(List<Signer> signer) {
        return signer.stream().map(s -> {
            DocumentSigner ds = new DocumentSigner();
            ds.setMail(s.getMail());
            ds.setName(s.getFullname());
            ds.setRole(s.getRole());
            ds.setCreated(DateTimeHelper.now());
            return ds;
        }).collect(Collectors.toList());
    }

    private ConversionHelpers() {
    }
}

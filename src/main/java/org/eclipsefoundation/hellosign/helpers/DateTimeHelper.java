/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.helpers;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Basic extension of existing date time helper to fill gaps in date time handling.
 */
public class DateTimeHelper {

    /**
     * Compares 2 zoned date time objects, using logic to truncate to a common accuracy point. This may be needed for
     * cases where the upstream data is only accurate to seconds rather than milliseconds.
     * 
     * @param first the first date to compare
     * @param second the second date to compare
     * @return true if the dates represent the same time to the second, truncating millis if present, false otherwise.
     */
    public static boolean looseCompare(ZonedDateTime first, ZonedDateTime second) {
        return (first == null && second == null) || (first != null && second != null
                && first
                        .withZoneSameInstant(ZoneOffset.UTC)
                        .toInstant()
                        .truncatedTo(ChronoUnit.SECONDS)
                        .compareTo(second.toInstant().truncatedTo(ChronoUnit.SECONDS)) == 0);
    }

    /**
     * Compares 2 local date time objects, using logic to truncate to a common accuracy point. This may be needed for
     * cases where the upstream data is only accurate to seconds rather than milliseconds.
     * 
     * @param first the first date to compare
     * @param second the second date to compare
     * @return true if the dates represent the same time to the second, truncating millis if present, false otherwise.
     */
    public static boolean looseCompare(LocalDateTime first, LocalDateTime second) {
        return (first == null && second == null) || (first != null && second != null
                && first
                        .toInstant(ZoneOffset.UTC)
                        .truncatedTo(ChronoUnit.SECONDS)
                        .compareTo(second.toInstant(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS)) == 0);
    }
}
